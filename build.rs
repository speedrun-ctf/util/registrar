use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    #[cfg(feature = "web")]
    {
        use std::process::Command;

        println!("cargo:rerun-if-changed=registrar-website/src");
        println!("cargo:rerun-if-changed=registrar-website/index.html");
        println!("cargo:rerun-if-changed=registrar-website/package.json");
        println!("cargo:rerun-if-changed=registrar-website/tsconfig.json");
        println!("cargo:rerun-if-changed=registrar-website/vite.config.ts");
        assert!(
            Command::new("yarn")
                .arg("install")
                .current_dir("registrar-website")
                .status()?
                .success(),
            "Couldn't install the website dependencies."
        );
        assert!(
            Command::new("yarn")
                .arg("build")
                .current_dir("registrar-website")
                .status()?
                .success(),
            "Couldn't build the website."
        );
    }

    Ok(())
}
